# Ansible - Workshop
Lab 04: Run Ad-Hoc modules

---
# Preperations

## Prepare hosts file - open host file

```
$ sudo vim /etc/ansible/hosts
```

## Paste the following snippet in the file - nodeip will be replaced by the managed node ip(or ip's) - <bold>if the hosts file is already updated, skip this step</bold>

```
[demoservers]
(nodeip(s))
```

## Save and quit
```
press esc
press :wq
```

# Instructions

- Run the service module
- Run the ping module
- Run the command module
- Run the command module without paramaeter

---

## Run the service module
```
$ ansible demoservers -m service -a “name=apache2 state=started”
```


## Run the ping module
```
$ ansible demoservers -m ping
```


## Run the command module
```
$ ansible demoservers -m command -a "echo  ‘Hello’”
```

## Run the command module without paramaeter
```
$ ansible demoservers -a "echo  ‘Hello’”
```